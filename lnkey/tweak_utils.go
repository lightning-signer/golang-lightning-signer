package lnkey

import (
	"crypto/sha256"
	"github.com/btcsuite/btcd/btcec"
)

// Copied from lnd

func SingleTweakBytes(commitPoint, basePoint *btcec.PublicKey) []byte {
	h := sha256.New()
	h.Write(commitPoint.SerializeCompressed())
	h.Write(basePoint.SerializeCompressed())
	return h.Sum(nil)
}

func TweakPubKeyWithTweak(pubKey *btcec.PublicKey, tweakBytes []byte) *btcec.PublicKey {
	curve := btcec.S256()
	tweakX, tweakY := curve.ScalarBaseMult(tweakBytes)

	// TODO(roasbeef): check that both passed on curve?
	x, y := curve.Add(pubKey.X, pubKey.Y, tweakX, tweakY)
	return &btcec.PublicKey{
		X:     x,
		Y:     y,
		Curve: curve,
	}
}
func TweakPubKey(basePoint, commitPoint *btcec.PublicKey) *btcec.PublicKey {
	tweakBytes := SingleTweakBytes(commitPoint, basePoint)
	return TweakPubKeyWithTweak(basePoint, tweakBytes)
}

func DeriveRevocationPubkey(revokeBase, commitPoint *btcec.PublicKey) *btcec.PublicKey {

	// R = revokeBase * sha256(revocationBase || commitPoint)
	revokeTweakBytes := SingleTweakBytes(revokeBase, commitPoint)
	rX, rY := btcec.S256().ScalarMult(revokeBase.X, revokeBase.Y,
		revokeTweakBytes)

	// C = commitPoint * sha256(commitPoint || revocationBase)
	commitTweakBytes := SingleTweakBytes(commitPoint, revokeBase)
	cX, cY := btcec.S256().ScalarMult(commitPoint.X, commitPoint.Y,
		commitTweakBytes)

	// Now that we have the revocation point, we add this to their commitment
	// public key in order to obtain the revocation public key.
	//
	// P = R + C
	revX, revY := btcec.S256().Add(rX, rY, cX, cY)
	return &btcec.PublicKey{
		X:     revX,
		Y:     revY,
		Curve: btcec.S256(),
	}
}
