# Overview

This document describes a set of policy controls that collectively ensure funds
are not lost to theft.  The controls are separated into a *mandatory* set, which
ensures the integrity of the basic Lightning protocol and an *optional* set, which includes
use-case specific controls.

Some controls will change with the evolution of the Lightning protocol - e.g.
[dual funding](https://github.com/lightningnetwork/lightning-rfc/pull/524).

# Mandatory Policy Controls
## Funding Transaction
* Output - the funding output must have the right scriptPubKey and be spendable by
  the previously signed initial commitment transaction (multi-output TBD)
* Change - the change output must be controlled by the layer-one wallet
* Fee - the fee must be reasonable
* Format - the transaction fields must be standard (e.g. version field)

## Commitment Transaction

Before we sign a commitment transaction, the following controls are checked:

* Input - the single input must spend the funding output
* Value - if we are the funder, the value to us of the initial commitment transaction
should be equal to our funding value 
* Format - version, locktime and nsequence must be as specified in BOLT 3
* Output - the outputs must be at most one to-local, at most one to-remote and HTLCs
* Funded - if this is not the first commitment, the funding UTXO must be active on chain
  with enough depth
* HTLC in-flight value - the inflight value should not be too large
* Fee - must be in range
* Number of HTLC outputs - must not be too large
* HTLC routing - each offered HTLC must be balanced via a received HTLC
* HTLC receive channel validity - the funding UTXO of the receive channel must be active on chain
  with enough depth
* Our revocation pubkey - must be correct
* To self delay and HTLC delay - must be within range
* Our payment pubkey - must be correct
* Our delayed payment pubkey - must be correct
* Our HTLC pubkey - must be correct
* Offered payment hash - must be related to received HTLC payment hash
* Trimming - outputs are trimmed iff under the dust limit
* Revocation - the previous commitment transaction was properly revoked by peer disclosing secret.  
  Note that this requires unbounded storage.
* No breach - if signing a local commitment transaction, we must not have revoked it

## Revocation

Before we revoke a commitment, the following controls are checked:

* New commitment - the remote must have signed the new commitment transaction
* No close - we did not sign a closing transaction

## HTLC Transactions

Before we sign an HTLC transaction, the following controls are checked:

* Delay - must be within range
* Our revocation pubkey - must be correct
* Our delayed payment pubkey - must be correct
* Format - version, locktime and nsequence must be as specified in BOLT 3
* Fee - must be in range

## Cooperative Closing Transaction

Before we sign a cooperative closing transaction, the following controls are checked:

* Destination - destination must be whitelisted
* Value - the value to us should be as in the last commitment transaction
* Fee - must be in range
* No pending HTLCs

## Uncooperative Closing Transaction
* Destination - destination must be whitelisted
* Fee - must be in range
* No close - we did not sign a different closing transaction

# Optional Policy Controls
## Funding Transaction
* Velocity - the amount funded must be under a certain amount per unit time  

## Commitment Transaction
* Velocity - the amount transferred to peer must be under a certain amount per unit time

# Use-case Specific Controls
## Merchant
* No sends - balances must only increase until closing or loop-out

## Routing Hub
* No sends - balances must only change via HTLC settlement
