# LiPoSig

Lightning Policy Signing

#### Golang Resources

secp256k1

1. github.com/btcsuite/btcd/btcec
2. github.com/ethereum/go-ethereum/crypto/secp256k1
3. github.com/decred/dcrd/dcrec/secp256k1
4. github.com/btccom/secp256k1-go
5. golang.org/pkg/crypto/elliptic/
6. github.com/piotrnar/gocoin/tree/master/lib/secp256k1

bitcoin util

1. github.com/btcsuite/btcutil
2. github.com/piotrnar/gocoin

BIP32

1. github.com/tyler-smith/go-bip32


#### Tips for c-lightning

    sudo rpm -e postgresql96-libs-9.6.15-1PGDG.f29.x86_64 postgresql96-9.6.15-1PGDG.f29.x86_64
    sudo dnf install postgresql postgresql-devel python-devel
    sudo dnf install bitcoin-0.18.1-1.fc29.x86_64.rpm

Running server with debug logging:

    export PYTHONPATH=`pwd`/hsmd

    lightningd --network=testnet --log-level=debug

Useful commands:

    lightning-cli newaddr
    lightning-cli listfunds
    lightning-cli listpeers
    lightning-cli connect <node_id> <ip> [<port>]
    lightning-cli fundchannel <node_id> <amount_in_satoshis>
    lightning-cli connect 03d5e17a3c213fe490e1b0c389f8cfcfcea08a29717d50a9f453735e0ab2a7c003@3.16.119.191:9735
    lightning-cli fundchannel 03d5e17a3c213fe490e1b0c389f8cfcfcea08a29717d50a9f453735e0ab2a7c003 100000
    lightning-cli close 03d5e17a3c213fe490e1b0c389f8cfcfcea08a29717d50a9f453735e0ab2a7c003

```
023f76b9c7f0aeaef5302b39126f028b75fda2ac6afa960c18c060d3677b2240b4@52.36.54.59:9735
03b882dcd309adaf4d66d1aadfbc6e85764bd65c6bdaf03689c55f1abd13f53fc5@95.216.101.105:9735
0303ba0fed62039df562a77bfcec887a9aa0767ff6519f6e2baa1309210544cd3d@5.9.150.112:9735
02ae2f22b02375e3e9b4b4a2db4f12e1b50752b4062dbefd6e01332acdaf680379@197.155.6.172:9735
037b50cc830191b42737dfc4ad021b6d2f9963b344620fd9c80d784dacaa729d18@76.95.133.92:9735
039b1717db1193eb332d3c0bfdcce90a6aab60efa478b60963d3b406a8fc45134a@172.81.180.180:9735
038863cf8ab91046230f561cd5b386cbff8309fa02e3f0c3ed161a3aeb64a643b9@203.132.95.10:9735
02db2dcb5a6d40980b77f286b0aff145c4fb70e5d9085ea9f910c54638703e36e5@3.17.183.85:9009
03a7606ffe3deea419b7d767932c8821b845bb33bf9603b0e3214ca6c52396f439@172.81.178.56:9735

```

#### Patterns

Startup:
```
lightningd --network=testnet --log-level=debug | grep 'Received message'
2019-10-30T19:55:34.012Z DEBUG lightning_hsmd(4134): Client: Received message 11 from client
2019-10-30T19:55:34.012Z DEBUG lightning_hsmd(4134): Client: Received message 9 from client
2019-10-30T19:55:34.097Z DEBUG lightning_hsmd(4134): Client: Received message 9 from client
2019-10-30T19:55:34.432Z DEBUG lightning_hsmd(4134): Client: Received message 10 from client
2019-10-30T19:55:34.561Z DEBUG lightning_hsmd(4134): Client: Received message 1 from client
2019-10-30T19:55:34.576Z DEBUG lightning_hsmd(4134): Client: Received message 9 from client
2019-10-30T19:55:34.598Z DEBUG lightning_hsmd(4134): Client: Received message 18 from client
2019-10-30T19:55:34.601Z DEBUG lightning_hsmd(4134): Client: Received message 18 from client
2019-10-30T19:55:34.605Z DEBUG lightning_hsmd(4134): Client: Received message 2 from client

WIRE_HSM_INIT = 11,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_GET_CHANNEL_BASEPOINTS = 10,
WIRE_HSM_ECDH_REQ = 1,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_GET_PER_COMMITMENT_POINT = 18,
WIRE_HSM_GET_PER_COMMITMENT_POINT = 18,
WIRE_HSM_CANNOUNCEMENT_SIG_REQ = 2,
```

connect:
```
2019-10-30T20:17:55.497Z DEBUG lightning_hsmd(4134): Client: Received message 1 from client
2019-10-30T20:17:55.594Z DEBUG lightning_hsmd(4134): Client: Received message 10 from client
2019-10-30T20:17:55.594Z DEBUG lightning_hsmd(4134): Client: Received message 9 from client
2019-10-30T20:17:55.668Z DEBUG lightning_hsmd(4134): Client: Received message 18 from client

WIRE_HSM_ECDH_REQ = 1,
WIRE_HSM_GET_CHANNEL_BASEPOINTS = 10,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_GET_PER_COMMITMENT_POINT = 18,
```

fundchannel:
```
2019-10-30T20:26:54.454Z DEBUG lightning_hsmd(4134): Client: Received message 19 from client
2019-10-30T20:26:54.575Z DEBUG lightning_hsmd(4134): Client: Received message 9 from client
2019-10-30T20:26:54.594Z DEBUG lightning_hsmd(4134): Client: Received message 7 from client
2019-10-30T20:26:54.594Z DEBUG lightning_hsmd(4134): Client: Received message 18 from client

WIRE_HSM_SIGN_REMOTE_COMMITMENT_TX = 19,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_SIGN_WITHDRAWAL = 7,
WIRE_HSM_GET_PER_COMMITMENT_POINT = 18,
```

close:
```
2019-11-01T21:48:47.710Z DEBUG lightning_hsmd(21413): Client: Received message 3 from client
2019-11-01T21:48:47.985Z DEBUG lightning_hsmd(21413): Client: Received message 9 from client
2019-11-01T21:48:47.985Z DEBUG lightning_hsmd(21413): Client: Received message 21 from client
2019-11-01T21:48:48.192Z DEBUG lightning_hsmd(21413): Client: Received message 5 from client
<maybe-block-here>
2019-11-01T21:49:10.219Z DEBUG lightning_hsmd(21413): Client: Received message 5 from client
2019-11-01T21:49:10.238Z DEBUG lightning_hsmd(21413): Client: Received message 9 from client

WIRE_HSM_CUPDATE_SIG_REQ = 3,
WIRE_HSM_CLIENT_HSMFD = 9,
WIRE_HSM_SIGN_MUTUAL_CLOSE_TX = 21,
WIRE_HSM_SIGN_COMMITMENT_TX = 5,
<maybe-block-here>
WIRE_HSM_SIGN_COMMITMENT_TX = 5,
WIRE_HSM_CLIENT_HSMFD = 9,
```

<spontaneous>:
```
2019-10-30T20:43:11.499Z DEBUG lightning_hsmd(4134): Client: Received message 3 from client

WIRE_HSM_CUPDATE_SIG_REQ = 3,
```

{
    'satoshi_out': {'satoshis': 100000},
    'change_out': {'satoshis': 805841},
    'change_keyindex': 56,
    'outputs': [
        {
            'amount': {'satoshis': 100000},
            'script': b'\x00 ?Tv\x7fZP9\x83\xc2{\xd7Ql\xd7\xf7V]\xc3\xdd\x1a\\\x9c)N\xdc\xe1\x0f9?G\x8e%'
        }
    ],
    'utxos': [
        {
            'txid': {'shad': {'sha': b'(\x1d\x97\xdeTz\x9c\x0bv\xfbOf\xb8\xb3!!\xd2 \x90s\x95\xc1Q\x95J\x99\x0c"\xd9\x9f\x8b\x81'}},
            'outnum': 0,
            'amount': {'satoshis': 99646},
            'keyindex': 36,
            'is_p2sh': False,
            'close_info': None
        },
        {
            'txid': {'shad': {'sha': b'\x80\xe2\xc9\xea\x8c\x1f\x129\xc7:F\xcfBS\xa9\x16\xc6\xe6\xbb\x94\xc8J\x11\xe5\xddj\x7f\x8f\xc2vqu'}},
            'outnum': 1,
            'amount': {'satoshis': 806418},
            'keyindex': 39,
            'is_p2sh': False,
            'close_info': None
        }
    ],
    'tx': {
        'input_amounts': [
            {'satoshis': 99646},
            {'satoshis': 806418}
        ],
        'wally_tx': {
            'version': 2,
            'locktime': 0,
            'inputs': [
                {
                    'txhash': b'(\x1d\x97\xdeTz\x9c\x0bv\xfbOf\xb8\xb3!!\xd2 \x90s\x95\xc1Q\x95J\x99\x0c"\xd9\x9f\x8b\x81',
                    'index': 0,
                    'sequence': 4294967295,
                    'script': b'',
                    'witness': None,
                    'features': 0
                },
                {
                    'txhash': b'\x80\xe2\xc9\xea\x8c\x1f\x129\xc7:F\xcfBS\xa9\x16\xc6\xe6\xbb\x94\xc8J\x11\xe5\xddj\x7f\x8f\xc2vqu',
                    'index': 1,
                    'sequence': 4294967295,
                    'script': b'',
                    'witness': None,
                    'features': 0
                }
            ],
            'inputs_allocation_len': 2,
            'outputs': [
                {
                    'satoshi': 100000,
                    'script': b'\x00 ?Tv\x7fZP9\x83\xc2{\xd7Ql\xd7\xf7V]\xc3\xdd\x1a\\\x9c)N\xdc\xe1\x0f9?G\x8e%',
                    'features': 0
                },
                {
                    'satoshi': 805841,
                    'script': b'\x00\x14\x1bO\x9ce\xa73\xfe\xcd3\x91\xe3\xfd\xd2\xc6\xf7\xc8\xe4p\xee\xf5',
                    'features': 0
                }
            ],
            'outputs_allocation_len': 2
        },
        'chainparams': {
            'network_name': 'testnet',
            'bip173_name': 'tb',
            'bip70_name': 'test',
            'genesis_blockhash': {'shad': {'sha': b'CI\x7f\xd7\xf8&\x95q\x08\xf4\xa3\x0f\xd9\xce\xc3\xae\xbay\x97 \x84\xe9\x0e\xad\x01\xea3\t\x00\x00\x00\x00'}},
            'rpc_port': 18332,
            'cli': 'bitcoin-cli',
            'cli_args': '-testnet',
            'cli_min_supported_version': 150000,
            'dust_limit': {'satoshis': 546},
            'max_funding': {'satoshis': 16777215},
            'max_payment': {'millisatoshis': 4294967295},
            'when_lightning_became_cool': 0,
            'p2pkh_version': 111,
            'p2sh_version': 196
        }
    }
}

```
# original
PYTHONPATH=`pwd`/hsmd:`pwd`/contrib/pylightning:`pwd`/contrib/pyln-testing:`pwd`/contrib/pyln-client:$PYTHONPATH \
TEST_DEBUG=1 \
DEVELOPER=0 \
VALGRIND=1 \
pytest tests/ -v --timeout=550 --timeout_method=thread -p no:logging -x

# Run a single test w/ stdout to terminal
PYTHONPATH=`pwd`/hsmd:`pwd`/contrib/pylightning:`pwd`/contrib/pyln-testing:`pwd`/contrib/pyln-client:$PYTHONPATH \
TEST_DEBUG=1 \
DEVELOPER=0 \
VALGRIND=0 \
ALT_SUBDAEMON='lightning_hsmd:remote_hsmd' \
pytest tests/test_connection.py::test_balance \
    -v --timeout=550 --timeout_method=thread -x -s \
|& tee log

# Test with channel sends
PYTHONPATH=`pwd`/hsmd:`pwd`/contrib/pylightning:`pwd`/contrib/pyln-testing:`pwd`/contrib/pyln-client:$PYTHONPATH \
TEST_DEBUG=1 \
DEVELOPER=0 \
VALGRIND=0 \
ALT_SUBDAEMON='lightning_hsmd:remote_hsmd' \
pytest tests/test_pay.py::test_sendpay \
-v --timeout=550 --timeout_method=thread -x -s \
|& tee log

# Test w/ a close
PYTHONPATH=`pwd`/hsmd:`pwd`/contrib/pylightning:`pwd`/contrib/pyln-testing:`pwd`/contrib/pyln-client:$PYTHONPATH \
TEST_DEBUG=1 \
DEVELOPER=0 \
VALGRIND=0 \
ALT_SUBDAEMON='lightning_hsmd:remote_hsmd' \
pytest tests/test_closing.py::test_closing_id \
-v --timeout=550 --timeout_method=thread -x -s |& tee log
```

PYTHONPATH=`pwd`/hsmd:`pwd`/contrib/pylightning:`pwd`/contrib/pyln-testing:`pwd`/contrib/pyln-client:$PYTHONPATH TEST_DEBUG=1 DEVELOPER=0 VALGRIND=0 pytest tests/test_misc.py::test_listconfigs     -v --timeout=550 --timeout_method=thread -x -s

        fprintf(stdout, "INPRIVKEY=");
        for (size_t ii = 0; ii < PRIVKEY_LEN; ++ii)
            fprintf(stdout, "%02x", inprivkey.secret.data[ii]);
        fprintf(stdout, "\n");
        fflush(stdout);
        
DEBUG:root:lightningd-1: INPRIVKEY=26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965
DEBUG:root:lightningd-1: INPRIVKEY=26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965
DEBUG:root:lightningd-1: INPRIVKEY=26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965

XPRV: tprv8eCzrcNxyQYRXgpBHRHiLjk3nxC7fVe8tzTa4xEWkssU1cPbbEJf7zn4r1bAfq8JdmkBEffbwtxX2u4rgcSikBoSWmpvrGFnXJs7F5i9o8y
XPRV: tprv8fAReKBKo3fRrXkVM88RU9BBwSdwdg6XHxdjEDK1v9u6WQeRh9Vz6tcBCtiiv5KuzCNaauJWsQeFsJWpMyWZDdVBysokaY3WjJooKWqbVAP

channeld: secp256k1_ecdsa_signature
channeld:     channel_txs
channeld:         commit_tx
channeld:     towire_hsm_sign_remote_commitment_tx
    hsmd: handle_sign_remote_commitment_tx

#### Early attempt at hash

liposig 17727bf43edf163ee8b4b81481c77ee7fee856a607b35c0418ee4e68d9c59be3

DEBUG:root:lightningd-1: HASH: e5de247f15782630c805b8d93ee4d87f0714c500f0fa16db878a36a737cb645f
DEBUG:root:lightningd-2: HASH: 3ef85b9921dc29aab246beb02be9101e096be396fae0ae80c1e6cb2ea561b91c
DEBUG:root:lightningd-1: HASH: 60a3edce866637ee61f712bcdbf987dabe5ca311bc70be5c1a73bf85dfcefa4b

#### Later attempt at sighash preimage￼

RUSTY 02000000bb068e70e65b3d6b54c343637f09d05fa4d03e42d9b1e5c1af5e6cf3098459533bb13029ce7b1f559ef5e747fcac439f1455a2ec7c5f09b72290795e70665044b47310c259e54becea88c310645603781c527447badb065961cc80608765ddae010000001976a91401fad90abcd66697e2592164722de4a95ebee16588ac80841e0000000000ffffffffa4937c38dee7174b7a79b552c1cd71de492edfc8a5baea85c1e76d83afe5cc590000000001000000

LIPOS 02000000bb068e70e65b3d6b54c343637f09d05fa4d03e42d9b1e5c1af5e6cf3098459533bb13029ce7b1f559ef5e747fcac439f1455a2ec7c5f09b72290795e70665044b47310c259e54becea88c310645603781c527447badb065961cc80608765ddae010000001976a91401fad90abcd66697e2592164722de4a95ebee16588ac80841e0000000000ffffffffa4937c38dee7174b7a79b552c1cd71de492edfc8a5baea85c1e76d83afe5cc590000000001000000

#### sighash

DEBUG:root:lightningd-1: HASH: 1a8c613e1679ff4fc820449b1ef8c79595127d448ca182113b3da3f282e87995
liposig debug 1a8c613e1679ff4fc820449b1ef8c79595127d448ca182113b3da3f282e87995

Lipo sig:
3045022100cbe8b1feb7d5d6b28afeca4eb6c76c72bbb41a61ae1e9a51ddc70b9a2b659de20220176c6f9c47986b64acaab19ee0b96395b14b1680b0fc3c460bf307bb98a1fe8801

#### witness stack (BAD)

DEBUG:root:lightningd-1: SIG 0: 3045022100cbe8b1feb7d5d6b28afeca4eb6c76c72bbb41a61ae1e9a51ddc70b9a2b659de20220176c6f9c47986b64acaab19ee0b96395b14b1680b0fc3c460bf307bb98a1fe8801, 03d745445c9362665f22e0d96e9e766f273f3260dea39c8a76bfa05dd2684ddccf
DEBUG:root:lightningd-1: REF 0: 30440220705d4f7a28804147c28a6340d3c7394bbd8d3f8e5e779ad02e103d79264608cd02206a42dd4113479c19434b24cf186e4d50a162100d0300a1890fbe55c7cabe9edd01, 03d745445c9362665f22e0d96e9e766f273f3260dea39c8a76bfa05dd2684ddccf

#### keys

keys are the same?
DEBUG:root:lightningd-1: INPRIVKEY: 26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965
2019/11/22 10:46:39 privKey "26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965"

#### Later (why did it change?) sighash (b RawTxInWitnessSignature)

020000008fd76962b76f4ea9a05b07ae30a8be362124a6a5e536f32935667dca11d395ac3bb13029ce7b1f559ef5e747fcac439f1455a2ec7c5f09b72290795e7066504484409a0090ac19226cfdebb56d5b9ba760ebad1782d4f528e37ed0ed676602b0010000001976a91401fad90abcd66697e2592164722de4a95ebee16588ac80841e0000000000ffffffffa4937c38dee7174b7a79b552c1cd71de492edfc8a5baea85c1e76d83afe5cc590000000001000000

Rusty:
020000008fd76962b76f4ea9a05b07ae30a8be362124a6a5e536f32935667dca11d395ac3bb13029ce7b1f559ef5e747fcac439f1455a2ec7c5f09b72290795e7066504484409a0090ac19226cfdebb56d5b9ba760ebad1782d4f528e37ed0ed676602b0010000001976a91401fad90abcd66697e2592164722de4a95ebee16588ac80841e0000000000ffffffffa4937c38dee7174b7a79b552c1cd71de492edfc8a5baea85c1e76d83afe5cc590000000001000000

liposig hash:
8fdb3e685ebdbdeb1659f4f82900361669a7b0f740d6818ec639a95b4527fc09

HASH: 8fdb3e685ebdbdeb1659f4f82900361669a7b0f740d6818ec639a95b4527fc09

liposig key:
26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965

INPRIVKEY: 26214d51df4595493e8fdcfc3a944d5d725584c6b78682a78e34b73a7d575965

liposig sig:
304402204d52492d98e4834d687d56f2e234ac40d40e0caadb8fd8901d3cf8568e4e73ec022062b9a7a2d3d9c1b6951258c0dc8583e41e983c2a86ee1d9c4cb8c13bb789adfc01

DEBUG:root:lightningd-1: SIG 0: 304402204d52492d98e4834d687d56f2e234ac40d40e0caadb8fd8901d3cf8568e4e73ec022062b9a7a2d3d9c1b6951258c0dc8583e41e983c2a86ee1d9c4cb8c13bb789adfc01, 03d745445c9362665f22e0d96e9e766f273f3260dea39c8a76bfa05dd2684ddccf
DEBUG:root:lightningd-1: REF 0: 3044022009e9edbcfc90435695f55177333984c8b6b994ae8f9d86989821fe238578a5f602200f10d55947c591dd616dc7f3b6a5b0c79b750177072128aa6680962737f1b57001, 03d745445c9362665f22e0d96e9e766f273f3260dea39c8a76bfa05dd2684ddccf

// grpc-devel.x86_64
// grpc-plugins.x86_64

protoc -I . --cpp_out=. api.proto
protoc -I . --grpc_out=. --plugin=protoc-gen-grpc=/usr/bin/grpc_cpp_plugin api.proto

g++  -I. -Iccan -DBINTOPKGLIBEXECDIR="\"../libexec/c-lightning\""  -c -o hsmd/proxy.o hsmd/proxy.cpp

