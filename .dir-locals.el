((go-mode . ((indent-tabs-mode . t)
             (show-trailing-whitespace . t)
             (c-basic-offset . 4)
             (tab-width . 4)
             ))
)

(add-hook 'before-save-hook 'gofmt-before-save)
