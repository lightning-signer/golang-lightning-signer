package server

import (
	"encoding/hex"
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil/hdkeychain"
	"gitlab.com/ksedgwic/json16"

	. "gitlab.com/lightning-signer/lightning-signer/remotesigner"
)

// Slices cannot be map keys, but arrays can.
func MakeChannelNonce(channelNonceBytes []byte) ChannelNonce {
	var channelNonce ChannelNonce
	copy(channelNonce[:], channelNonceBytes)
	return channelNonce
}

type ChannelState struct {
	Capabilities uint64
	N            uint64
}

// Slices cannot be map keys, but arrays can.
type NodeIdVal [33]byte

// NodeIdVal should print as hex
func (nid NodeIdVal) String() string {
	return hex.EncodeToString(nid[:])
}

// Slices cannot be map keys, but arrays can.
func MakeNodeIdVal(nodeIdBytes []byte) NodeIdVal {
	var nodeId NodeIdVal
	copy(nodeId[:], nodeIdBytes)
	return nodeId
}

type NodeState struct {
	// Saved from InitHSM
	NodeChainParams *ChainParams
	// HSM secret
	// TODO might not be needed after deriving other fields in this structure
	Secret []byte
	// Matches the published node public key
	PrivKey *btcec.PrivateKey
	// Used internally to derive layer one keys
	BIP32Key *hdkeychain.ExtendedKey
	// Used internally to derive per-channel secrets and keys
	ChannelSeedBase []byte

	// Current state for each channel
	Channels ChannelMap
}

// Slices cannot be map keys, but arrays can.
type ChannelNonce [41]byte // sizeof(NodeIdVal) + sizeof(uint8)

// ChannelNonce should print as hex
func (cn ChannelNonce) String() string {
	return hex.EncodeToString(cn[:])
}

type ChannelMap map[ChannelNonce]*ChannelState

// Hack to allow ChannelMaps to be JSON serialized
func (cm *ChannelMap) MarshalJSON() ([]byte, error) {
	prx := map[string]*ChannelState{}
	for kk, vv := range *cm {
		prx[kk.String()] = vv
	}
	return json16.Marshal(prx)
}

type NodeMap map[NodeIdVal]*NodeState

// Hack to allow NodeMaps to be JSON serialized
func (nm *NodeMap) MarshalJSON() ([]byte, error) {
	prx := map[string]*NodeState{}
	for kk, vv := range *nm {
		prx[kk.String()] = vv
	}
	return json16.Marshal(prx)
}

func NodeChainParams(node *NodeState) *chaincfg.Params {
	var chainParams = &chaincfg.MainNetParams
	if node.NodeChainParams.Testnet {
		chainParams = &chaincfg.TestNet3Params
	}
	return chainParams
}
