package server

type Policy struct {
	Enforce      bool
	MaximumDelay uint
	MinimumDelay uint
}

var policy = Policy{
	Enforce:      true,
	MinimumDelay: 5, MaximumDelay: 250,
}
