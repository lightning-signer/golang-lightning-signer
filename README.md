# LiPoSig

**Lightning Policy Signing**

liposig is a reference implementation of an external policy signer
for the Lightning protocol.

## Motivation

Lightning nodes are hot wallets, because of the need to keep
keys online to participate in the protocol.  This presents a security
challenge.

See more in [this blog post](https://medium.com/@devrandom/securing-lightning-nodes-39410747734b).

## Goals

The goals of the external policy signer are:

* separate the signing logic and other handling of secrets from the Lightning node,
minimizing the attack surface
* potentially move the signing process into a hardware security module
* potentially split the signing process into multiple collaborating signers,
eliminating the single point of failure
* enumerate the policies that the signer must enforce
so that the Lightning node does not have to be trusted

## Project Status

This is a proof-of-concept, so that the approach can be evaluated.

The [list of controls](docs/policy-controls.md) is meant to be exhaustive.

Only a subset of the API calls and a subset of controls are
implemented.
 
## The Signer API

See the [gRPC definition](remotesigner/remotesigner.proto).

## Getting Started

Requirements include the **proto3** compiler and **go 1.13**.

The daemon currently integrates with a fork of c-lightning, described elsewhere.  To build and run
the daemon:

```bash
make
./lightning-signer
```
