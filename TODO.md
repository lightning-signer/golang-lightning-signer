
#### Consider not having separate fields in ChannelUpdateSigReq.

See BOLT #7 https://github.com/lightningnetwork/lightning-rfc/blob/master/07-routing-gossip.md#the-channel_announcement-message


#### Reconcile Signature Returns

There are different kinds of signatures:

  1. witness stack signatures
  2. secp256k1_ecdsa_signature
  
  Sometimes we return multiple signatures (one per input).
  Sometimes we reutrn only a single signature (or small fixed number).
  
  Consider identifying different signature types w/ protobuf messages.
  Consider reconciling single vs variable number signature returns.

  
#### Should most 'bytes' fields in protobuf be trivially typed?

Would imporove safety (many uses are actually fixed format and could
be checked).

Improves readability, reuse and debugging.


#### Consider Renaming Methods

The current methods get their names from c-lightning, some are bad.
Use the BOLT docs to find best standard names for each.


#### Reorder Args

Current order is inconsistent.


#### (style) Maybe API Arrangement (ordering) Should Mirror BOLTs?

Not sure if each API is associated w/ a single BOLT.
