package lnscript

import (
	"errors"
	"fmt"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"

	"gitlab.com/lightning-signer/lightning-signer/logging"
)

type CommitmentInfo struct {
	ToRemoteAddress   btcutil.Address
	ToRemoteValue     int64
	ToLocalDelayedKey *btcec.PublicKey
	ToLocalValue      int64
	RevocationKey     *btcec.PublicKey
	Delay             uint
	// TODO HTLC info
}

// To-local is more than dust
func (commitment *CommitmentInfo) HaveToLocal() bool {
	return commitment.ToLocalDelayedKey != nil
}

// To-remote is more than dust
func (commitment *CommitmentInfo) HaveToRemote() bool {
	return commitment.ToRemoteAddress != nil
}

// Consume an output and classify it based on its structure
func (commitment *CommitmentInfo) HandleOutput(out *wire.TxOut, witscript []byte) error {
	class, addrs, _, err := txscript.ExtractPkScriptAddrs(out.PkScript, &chaincfg.TestNet3Params)
	if err != nil {
		return err
	}

	if class == txscript.WitnessV0PubKeyHashTy {
		if commitment.ToRemoteAddress != nil {
			return errors.New("more than one to_remote")
		}
		commitment.ToRemoteAddress = addrs[0]
		commitment.ToRemoteValue = out.Value
		return nil
	} else if class == txscript.WitnessV0ScriptHashTy {
		if witscript == nil {
			return errors.New("output is p2wsh, but no sub-script provided")
		}

		err = commitment.handleToLocalScript(witscript)
		if err == nil {
			commitment.ToLocalValue = out.Value
		}

		if err != nil {
			err = commitment.handleOfferedHTLCScript(witscript)
			if err == nil {
				logging.LogIt("offered HTLC", out.Value)
			}
		}

		if err != nil {
			err = commitment.handleReceivedHTLCScript(witscript)
			if err == nil {
				logging.LogIt("received HTLC", out.Value)
			}
		}

		return err
	} else {
		return errors.New(fmt.Sprintf("unknown output type %d", class))
	}
}

func (commitment *CommitmentInfo) handleToLocalScript(script []byte) error {
	cursor := 0
	cursor, err := ExpectOpcode(script, cursor, txscript.OP_IF)
	if err != nil {
		return err
	}

	revocationKeyBytes, cursor, err := ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcode(script, cursor, txscript.OP_ELSE)
	if err != nil {
		return err
	}

	delay, cursor, err := ParsePushInt(script, cursor)
	cursor, err = ExpectOpcode(script, cursor, txscript.OP_CHECKSEQUENCEVERIFY)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcode(script, cursor, txscript.OP_DROP)
	if err != nil {
		return err
	}

	toLocalDelayedKeyBytes, cursor, err := ParseOpData(script, cursor)
	if err != nil {
		return err
	}
	cursor, err = ExpectOpcode(script, cursor, txscript.OP_ENDIF)
	if err != nil {
		return err
	}
	cursor, err = ExpectOpcode(script, cursor, txscript.OP_CHECKSIG)
	if err != nil {
		return err
	}
	if cursor != len(script) {
		return parseError
	}
	if commitment.RevocationKey != nil {
		return errors.New("already parsed to_local")
	}
	if delay < 0 {
		return errors.New("negative CSV delay")
	}
	if delay > 1000 {
		return errors.New("delay > 1000")
	}
	commitment.Delay = uint(delay)
	commitment.RevocationKey, err = btcec.ParsePubKey(revocationKeyBytes, btcec.S256())
	if err != nil {
		return nil
	}
	commitment.ToLocalDelayedKey, err = btcec.ParsePubKey(toLocalDelayedKeyBytes, btcec.S256())
	if err != nil {
		return nil
	}
	return nil
}

func ExpectOpcodes(script []byte, cursor int, codes []byte) (int, error) {
	for i := range codes {
		var err error
		cursor, err = ExpectOpcode(script, cursor, codes[i])
		if err != nil {
			return cursor, err
		}
	}
	return cursor, nil
}

func (commitment *CommitmentInfo) handleOfferedHTLCScript(script []byte) error {
	cursor := 0
	cursor, err := ExpectOpcodes(script, cursor, []byte{txscript.OP_DUP, txscript.OP_HASH160})
	if err != nil {
		return err
	}

	// Revocation hash
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUAL, txscript.OP_IF, txscript.OP_CHECKSIG, txscript.OP_ELSE})
	if err != nil {
		return err
	}

	// Remote HTLC pubkey
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_SWAP, txscript.OP_SIZE})
	if err != nil {
		return err
	}

	thirtyTwo, cursor, err := ParsePushInt(script, cursor)
	if err != nil {
		return err
	}

	if thirtyTwo != 32 {
		return parseError
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUAL, txscript.OP_NOTIF, txscript.OP_DROP, txscript.OP_2, txscript.OP_SWAP})
	if err != nil {
		return err
	}

	// Local HTLC pubkey
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_2, txscript.OP_CHECKMULTISIG, txscript.OP_ELSE, txscript.OP_HASH160})
	if err != nil {
		return err
	}

	// Payment hash
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUALVERIFY, txscript.OP_CHECKSIG, txscript.OP_ENDIF, txscript.OP_ENDIF})
	if err != nil {
		return err
	}

	return nil
}

func (commitment *CommitmentInfo) handleReceivedHTLCScript(script []byte) error {
	cursor := 0
	cursor, err := ExpectOpcodes(script, cursor, []byte{txscript.OP_DUP, txscript.OP_HASH160})
	if err != nil {
		return err
	}

	// Revocation hash
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUAL, txscript.OP_IF, txscript.OP_CHECKSIG, txscript.OP_ELSE})
	if err != nil {
		return err
	}

	// Remote HTLC pubkey
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_SWAP, txscript.OP_SIZE})
	if err != nil {
		return err
	}

	thirtyTwo, cursor, err := ParsePushInt(script, cursor)
	if err != nil {
		return err
	}

	if thirtyTwo != 32 {
		return parseError
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUAL, txscript.OP_IF, txscript.OP_HASH160})
	if err != nil {
		return err
	}

	// Payment hash
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_EQUALVERIFY, txscript.OP_2, txscript.OP_SWAP})
	if err != nil {
		return err
	}

	// Local HTLC pubkey
	_, cursor, err = ParseOpData(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_2, txscript.OP_CHECKMULTISIG, txscript.OP_ELSE, txscript.OP_DROP})
	if err != nil {
		return err
	}

	// Delay
	_, cursor, err = ParsePushInt(script, cursor)
	if err != nil {
		return err
	}

	cursor, err = ExpectOpcodes(script, cursor,
		[]byte{txscript.OP_CHECKLOCKTIMEVERIFY, txscript.OP_DROP, txscript.OP_CHECKSIG, txscript.OP_ENDIF, txscript.OP_ENDIF})
	if err != nil {
		return err
	}

	return nil
}

func (commitment *CommitmentInfo) String() string {
	return fmt.Sprintf("to_local=%d to_remote=%d delay=%d",
		commitment.ToLocalValue, commitment.ToRemoteValue, commitment.Delay)
}
