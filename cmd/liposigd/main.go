package main

import (
	"fmt"
	"os"

	flags "github.com/jessevdk/go-flags"

	liposig "gitlab.com/lightning-signer/lightning-signer"
)

func main() {
	if err := liposig.Main(); err != nil {
		if e, ok := err.(*flags.Error); ok && e.Type == flags.ErrHelp {
		} else {
			fmt.Fprintln(os.Stderr, err)
		}
		os.Exit(1)
	}
}
